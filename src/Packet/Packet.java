package Packet;

/**
 * Created by MichaelS on 2018-04-22.
 */
public class Packet {
    private int id;
    private String body;

    public Packet(String message){
        //char idSetter = message.charAt(0);
        String idSetter = message.substring(0,4);

        if(message.charAt(0)=='/') {
            switch (idSetter) {
                case "/hel":
                    this.id = 1;
                    break;
                case "/art":
                    this.id = 2;
                    break;
                case "/woo":
                    this.id = 3;
                    break;
                case "-END":
                    this.id = 4;
                    break;
                default:
                    this.id = 5;
                    break;
            }
        }else {
            this.id = 0;
        }
        this.body = message;
    }

    public int getId() {
        return id;
    }

    public String getBody() {
        return body;
    }


}

package General.Server.Client;

import General.Server.GeneralChatWindow;
import General.Server.Server.Server;
import Packet.Packet;

import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by MichaelS on 2018-04-22.
 */
public class Client extends GeneralChatWindow{

    private String serverIP;

    public Client(String IPServer){
        super();
        serverIP = IPServer;
        setUserName("Client");
        setOtherName("Server");
        setTitle(getUserName());

    }

    @Override
    public void waitForConnection() throws IOException {
        showMessage("*Connecting to Server....");
        setConnection(new Socket(InetAddress.getByName(serverIP), 47095));
        showMessage("\n *Successfully connected to server : " + serverIP);
    }

    @Override
    public void runServer(){
        try{

            while(isConnectionMade()){

                try{
                    waitForConnection();
                    setUpStreams();
                    chatting();
                }catch(EOFException eofException){
                    showMessage("\n *" + this.getUserName() + " ended connection \n");
                }finally {
                    closeServer();
                }
            }
        }catch(IOException ioException){
            ioException.printStackTrace();
        }
    }

}

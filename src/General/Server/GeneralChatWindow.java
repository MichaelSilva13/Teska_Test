/*
 * Created by JFormDesigner on Sun Apr 22 16:49:55 EDT 2018
 */

package General.Server;

import javax.swing.border.TitledBorder;
import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.*;
import javax.swing.JFrame;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import Packet.Packet;

/**
 * @author Michael Oliveira-Silva
 * Made with help of tutorial on youtube by user : thenewboston, aka Bucky Roberts
 */
public class GeneralChatWindow extends JFrame {


    private ObjectOutputStream output; //ourOutputStream
    private ObjectInputStream input; //InputStream
    private ServerSocket server; //ourServer
    private Socket connection; //OurConnection
    private String name, otherName;
    private Packet message;
    private boolean connectionMade = true;

    //Runs
    public void runServer(){
        try{

            setUpServer();

            while(connectionMade){

                try{
                    waitForConnection();
                    setUpStreams();
                    chatting();
                }catch(EOFException eofException){
                    showMessage("\n *" + name + " ended connection \n");
                }finally {
                    closeServer();
                }
            }
        }catch(IOException ioException){
            ioException.printStackTrace();
        }
    }

    public void  setUpServer() throws IOException{
        server = new ServerSocket(47095, 100);
    }

    private void sendBtnActionPerformed(ActionEvent e) {
        message = new Packet(inputText.getText());
        sendMessage(message.getBody());
        inputText.setText("");
    }

    //sends message to the server
    public void sendMessage(String message){

        try{
            output.writeObject(message);
            output.flush();
            showMessage("\n" + name + ": " + message);
        }catch(IOException ioException){
            chatWindow.append("\n *ERROR COULD NOT SEND MESSAGE");
        }

    }

    //shows Message in chatlog
    public void showMessage(String message){
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        chatWindow.append(message);
                    }
                }
        );
    }

    public void waitForConnection() throws IOException {
        showMessage("*Waiting for connection... \n");
        connection = server.accept();
        showMessage("*Now connected to " + connection.getInetAddress().getHostName());
    }

    //set Up the Stream
    public void setUpStreams() throws IOException{

        output = new ObjectOutputStream(connection.getOutputStream());
        output.flush(); //clean up in case of left overs
        input = new ObjectInputStream(connection.getInputStream());

        showMessage("\n *Streams set up* \n");

        if(name.equals("Server")) {
            this.message = new Packet("*Welcome to the Chat room. To ask for help you can use the command '/hel.'" +
                                      "Other commands are '/art', '/woo' " +
                                      "Stop the connection enter the '-END'.\n");
            sendMessage(message.getBody());
        }

    }

    //Shows incoming messages
    //TODO not forget Packets
    public void chatting() throws IOException{
        this.message = new Packet(" *Connection set!* \n");
        showMessage(message.getBody());
        ableToType(true);
        do{
            try{
                String message = (String) input.readObject();
                this.message = new Packet(message);
                showMessage("\n" + otherName + ": " + message);
            }catch(ClassNotFoundException classNotFoundException){
                showMessage("\n *Not a valid message*");
            }
        }while(!message.getBody().equals("-END"));

    }

    public void ableToType(boolean b) {
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        inputText.setEditable(b);
                    }
                }
        );

    }

    public void closeServer(){
        showMessage("\n *Closing connection... \n");
        ableToType(false);
        try{
            output.close();
            input.close();
            connection.close();
            connectionMade = false;

        }catch(IOException ioException){
            ioException.printStackTrace();
        }

    }

    private void inputTextActionPerformed(ActionEvent e) {
        this.message = new Packet(inputText.getText());
        sendMessage(message.getBody());
        inputText.setText("");
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        this.inputText = new JTextField();
        this.scrollPane1 = new JScrollPane();
        this.chatWindow = new JTextArea();
        this.sendBtn = new JButton();

        //======== this ========
        setName("this");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- inputText ----
        this.inputText.setEditable(false);
        this.inputText.setName("inputText");
        this.inputText.addActionListener(e -> inputTextActionPerformed(e));
        contentPane.add(this.inputText);
        this.inputText.setBounds(15, 280, 465, 34);

        //======== scrollPane1 ========
        {
            this.scrollPane1.setName("scrollPane1");

            //---- chatWindow ----
            this.chatWindow.setEditable(false);
            this.chatWindow.setLineWrap(true);
            this.chatWindow.setBorder(null);
            this.chatWindow.setName("chatWindow");
            this.scrollPane1.setViewportView(this.chatWindow);
        }
        contentPane.add(this.scrollPane1);
        this.scrollPane1.setBounds(15, 15, 540, 250);

        //---- sendBtn ----
        this.sendBtn.setText("Send!");
        this.sendBtn.setName("sendBtn");
        this.sendBtn.addActionListener(e -> sendBtnActionPerformed(e));
        contentPane.add(this.sendBtn);
        this.sendBtn.setBounds(480, 270, 80, 50);

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    public void setServer(ServerSocket server) {
        this.server = server;
    }


    public void setConnection(Socket connection) {
        this.connection = connection;
    }


    public String getUserName() {
        return name;
    }


    public void setUserName(String name) {
        this.name = name;
    }


    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public Packet getMessage() {
        return message;
    }

    public void setMessage(Packet message) {
        this.message = message;
    }

    public ObjectInputStream getInput() {
        return input;
    }

    public boolean isConnectionMade() {
        return connectionMade;
    }

    public String getOtherName() {
        return otherName;
    }

    //Constructor
    public GeneralChatWindow() {
        initComponents();
        this.setVisible(true);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JTextField inputText;
    private JScrollPane scrollPane1;
    private JTextArea chatWindow;
    private JButton sendBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}

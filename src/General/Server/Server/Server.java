/*
 * Created by JFormDesigner on Sun Apr 22 15:21:50 EDT 2018
 */

package General.Server.Server;

import General.Server.GeneralChatWindow;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.JFrame;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import Packet.Packet;

/**
 * @author Michael Oliveira-Silva
 */
public class Server extends GeneralChatWindow {

    private String art = "\n" +
            "          , \n" +
            "        0/ \n" +
            "       /7, \n" +
            "     .'( \n" +
            "   '^ / > \n" +
            "     / <  SK \n" +
            "     ` " ;
    private String woof = "\n     |\\_/|                  \n" +
                          "     | @ @   Woof! \n" +
                          "     |   <>              _  \n" +
                          "     |  _/\\------____ ((| |))\n" +
                          "     |               `--' |   \n" +
                          " ____|_       ___|   |___.' \n" +
                          "/_/_____/____/_______|";
    private String help = "*To end send '-END' to see cool stuff send '/woof' or '/art'";

    private String badCommand = "Please enter a valid command";

    //constructeur
    public Server() {
        super();
        setUserName("Server");
        setOtherName("Client");
        setTitle(getUserName());

    }

    @Override
    public void  setUpServer() throws IOException{
        setServer(new ServerSocket(47095, 2));
    }

    private void serverAutoResponse(Packet message){
        int messageId = message.getId();
        Packet autoMessage;
        switch (messageId){
            case 1:
                autoMessage = new Packet(help);
                sendMessage(autoMessage.getBody());
                break;
            case 2:
                autoMessage = new Packet(art);
                sendMessage(autoMessage.getBody());
                break;
            case 3:
                autoMessage = new Packet(woof);
                sendMessage(autoMessage.getBody());
                break;
            case 5:
                autoMessage = new Packet(badCommand);
                sendMessage(autoMessage.getBody());
                break;
            default:
                break;

        }
    }

    @Override
    public void chatting() throws IOException{
        setMessage(new Packet(" *Connection set!* \n"));
        showMessage(getMessage().getBody());
        ableToType(true);
        do{
            try{
                String message = (String) getInput().readObject();
                setMessage(new Packet(message));
                showMessage("\n" + getOtherName() + ": " + message);
                serverAutoResponse(getMessage());
                System.out.println(getMessage().getId()+"");
            }catch(ClassNotFoundException classNotFoundException){
                showMessage("\n *Not a valid message*");
            }
        }while(!getMessage().getBody().equals("-END"));


    }

    //initialise les composants du frame
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license

        //======== this ========
        setName("this");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
